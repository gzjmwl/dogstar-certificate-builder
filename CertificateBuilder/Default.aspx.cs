﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Security.Cryptography.Utility;

namespace CertificateBuilder
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        private string PublicKey = "<RSAKeyValue><Modulus>smq2B+TFPwhBs1tVzHm3oIV9KsXzC33KTaA54n/0bnJHieeslQnv312kXd0bvxQJqFpRI0qsF4fShFcTGny42M/Lw00twOYIxGQNOcjX7K3P09iNxrq/hC43sdtrrhI6ylQXaOi2jaFnhMoPVkzoMXJbopczTdlr4bJIFa9+mIfXMDz23R/f8lCH+Crfa2XGEEdik5aPEZumYeS+jijqi6dTx3xUU3pqg0h+mLT+WYeFYfY8WWMYHtPDwklvtltX7yu/IAbh07MmkSVoMkRWKx2cjxJT/2+XyNi9n/620P7OOBTchCVKkyyyMxqzaGhnAGnyB2U0hnZZj289YlnI6BENwkzLqotJcfq5bUwNGQ2gBWHtu5fzxM4kSCbnLOYMi1BHn+JoC9t2YTRO7De0F9Vy0gCG5ipkU7eK2HAqbyj8rnSL9/EyuBdcK1JoLtHoDRZMh0+Y0seeRNXSJ9CYTUO45QIYUYP6uUsJ2Y7I7xLLrvar59tGM09sfPp3UAUjHXYsjAYkTjg1Bzh63p5a1cTVKMLiATQ/F84fFXvgaUBgElbek3boT9Wq5ivN8tuRYShdUIc/RGPaNhb6C+iMg3qFTIbH4AyjwSXjqPMscQ8U38hfqVEn8XpPkDZm8bT9NIxlJFJtOyLUv9OAsP1mQ7IRppF9lvUEmNBAyu+dXcU=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            StringBuilder temp = new StringBuilder();
            try
            {
                if (!IsValid) { throw new Exception("无效的数据！"); }
                DateTime startDate, endDate;
                if (!DateTime.TryParse(txtStartDate.Text, out startDate) || !DateTime.TryParse(txtTimeExpired.Text, out endDate))
                { throw new Exception("无效的日期！"); }
                int version;
                int.TryParse(txtVersion.Text, out version);
                Guid key = Guid.NewGuid();

                string[] domains = txtDomain.Text.Trim().Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries);
                temp.AppendLine(@"<?xml version=""1.0""?>");
                temp.Append("<certificate");
                temp.AppendFormat(" data=\"{0}\">"
                    , HexEncoding.GetString(AsymmetricEncryption.EncryptData(string.Format("{0}:{1:yyyy-MM-dd},{2:yyyy-MM-dd},{3}{4}"
                    , key, startDate, endDate,version
                    , string.IsNullOrWhiteSpace(txtIP.Text) ? "" : "," + txtIP.Text.Trim())
                    , PublicKey)));
                temp.AppendLine();
                temp.AppendLine("<domains>");
                foreach (string domain in domains)
                {
                    temp.Append("<add>");
                    temp.Append(HexEncoding.GetString(AsymmetricEncryption.EncryptData(string.Format("{0}:{1}"
                        , key, domain), PublicKey)));
                    temp.Append("</add>");
                }
                temp.AppendLine("</domains>");
                if (!string.IsNullOrWhiteSpace(txtCompanyName.Text))
                {
                    temp.Append("<company>");
                    temp.Append(HexEncoding.GetString(AsymmetricEncryption.EncryptData(string.Format("{0}:{1}"
                      , key, txtCompanyName.Text.Trim())
                        , PublicKey)));
                    temp.Append("</company>");
                }
                temp.Append("<password>");
                temp.Append(HexEncoding.GetString(AsymmetricEncryption.EncryptData(string.Format("{0}:{1}"
                    , key, txtPassword.Text.Trim())
                    , PublicKey)));
                temp.Append("</password>");

                temp.AppendLine("</certificate>");

                Page.Response.AddHeader("Content-disposition"
        , "attachment; filename=Certificate.config");
                Page.Response.ContentType = "text/xml";
                Page.Response.Write(temp);
                Page.Response.End();
            }
            catch (Exception exp)
            {
                temp.Clear();
                temp.AppendLine("<script type=\"text/javascript\">alert(\"" + exp.Message + "\")</script>");
                Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", temp.ToString(), false);
            }
           
        }
    }
}
