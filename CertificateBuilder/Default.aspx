﻿<%@ Page Title="主页" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="CertificateBuilder._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script src="Scripts/Date/WdatePicker.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        证书生成器
    </h2>
        <asp:Panel ID="MainPanel" runat="server"  Width="100%">
            <table border="0" width="100%">
             <tr>
                    <td style="text-align: left">
                      公司名称：</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtCompanyName" runat="server"
						 Width="300px"></asp:TextBox>
                             <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
        runat="server"  Display="Dynamic" ControlToValidate="txtCompanyName" ForeColor="Red" 
        ErrorMessage="最长只支持255个字符" 
        ValidationExpression="^.{1,255}$"></asp:RegularExpressionValidator>
                         </td>
                </tr>

                <tr>
                    <td style="text-align: left">
                       域名：</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtDomain" runat="server"
						 Width="300px"></asp:TextBox><span style="color:Red;">*</span>
                         <asp:RequiredFieldValidator ID="rqvDomain" ControlToValidate="txtDomain"  Display="Dynamic" ForeColor="Red"
                             runat="server" ErrorMessage="必填的"></asp:RequiredFieldValidator>
                         
                         </td>
                </tr>
                <tr>
                <td></td>
                <td>可以省去“www”，多个域名用英文逗号分隔,如baidu.com,baidu.cn</td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        IP：</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtIP" runat="server" 
						Width="300px"></asp:TextBox>
                        </td>
                </tr>
                <tr>
                <td></td>
                <td>系统安装的服务器IP地址</td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        使用日期：</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtStartDate" runat="server" ClientIDMode="Static"  CssClass="Wdate" onfocus="WdatePicker({skin:'whyGreen'});" 
                        Width="300px"></asp:TextBox><span style="color:Red;">*</span>
                         <asp:RequiredFieldValidator ID="rqvStartDate" ControlToValidate="txtStartDate" Display="Dynamic" ForeColor="Red"
                             runat="server" ErrorMessage="必填的"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        有效期至：</td>
                    <td style="text-align: left">
                   
                        <asp:TextBox ID="txtTimeExpired" runat="server"  CssClass="Wdate" ClientIDMode="Static" 
                        onfocus="WdatePicker({skin:'whyGreen',minDate:'#F{$dp.$D(\'txtStartDate\')}'})" 
                        Width="300px"></asp:TextBox><span style="color:Red;">*</span>
                         <asp:RequiredFieldValidator ID="rqvTimeExpired" ControlToValidate="txtTimeExpired" Display="Dynamic" ForeColor="Red"
                             runat="server" ErrorMessage="必填的"></asp:RequiredFieldValidator></td>
                </tr>
                <tr>
                <td></td>
                <td>服务的最后期限</td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        版本号：</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtVersion" runat="server" ></asp:TextBox><span style="color:Red;">*</span>
                         <asp:RequiredFieldValidator ID="reqVersion" ControlToValidate="txtVersion" Display="Dynamic" ForeColor="Red"
                             runat="server" ErrorMessage="必填的"></asp:RequiredFieldValidator>
                     
    <asp:RegularExpressionValidator ID="rgxVersion"
        runat="server"  Display="Dynamic" ControlToValidate="txtVersion" ForeColor="Red" 
        ErrorMessage="必须是数字" 
        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                             </td>
                </tr>

               <tr>
                    <td style="text-align: left">
                        证书密码：</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtPassword" runat="server" ></asp:TextBox><span style="color:Red;">*</span>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtPassword" Display="Dynamic" ForeColor="Red"
                             runat="server" ErrorMessage="必填的"></asp:RequiredFieldValidator>
                     
    <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
        runat="server"  Display="Dynamic" ControlToValidate="txtPassword" ForeColor="Red" 
        ErrorMessage="长度必须是8到16位" 
        ValidationExpression="^.{8,16}$"></asp:RegularExpressionValidator>
                             </td>
                </tr>

               <tr>
                    <td style="text-align: left">
                        确认证书密码：</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtConfirm" runat="server" ></asp:TextBox><span style="color:Red;">*</span> 
                        
            <asp:CompareValidator ID="CompareValidator1" Display="Dynamic" ControlToValidate="txtConfirm" 
             ControlToCompare="txtPassword" Operator="Equal" Type="String" ForeColor="Red" runat="server" ErrorMessage="两次输入密码必须完全一至"></asp:CompareValidator>
             </td>
                </tr>


                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="提交" onclick="btnSubmit_Click" />
                      </td>
                </tr>
            </table>
           <asp:Label ID="lbl" runat="server"></asp:Label>
        </asp:Panel>
</asp:Content>
